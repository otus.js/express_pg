const typeorm = require('typeorm');

module.exports = {
    default: new typeorm.DataSource({
        type: 'postgres',
        // host: '127.0.0.1',
        host: 'pg_db',
        port: 5432,
        username: 'postgres',
        password: 'password',
        database: 'postgres',

        entities: ['./models/*.js'],
         migrations: ['./migrations/*.js'],
         migrationsTableName: '__migrations'
    })
};
