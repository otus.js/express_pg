const express = require("express");
const cors = require("cors");

const { MoreThan, Like, createConnection } = require('typeorm');

const start = async () => {
    const app = express();
    var corsOptions = {
        origin: "http://localhost:3001"
    };
    app.use(cors(corsOptions));

    app.use(express.json());

    //Инициализация БД
    const DS = require('./datasource.js').default;
    await DS.initialize()
      .then(() => {console.log("Synced db.");
      })
    .catch ((err) => {
        console.log("Error sync db: " + err.message);
    });

    app.use(express.urlencoded({ extended: true }));

    //swagger
    const swaggerJSDoc = require('swagger-jsdoc');
    const swaggerDefinition = {
        openapi: '3.0.0',
        info: {
            title: 'Express API',
            version: '1.0.0',
        },
    };
    const options = {
        swaggerDefinition,
        apis: ['./routes/*.js'],
    };
    const swaggerSpec = swaggerJSDoc(options);
    const swaggerUi = require('swagger-ui-express');

    app.use("/api", swaggerUi.serve, swaggerUi.setup(swaggerSpec));

    // simple route
    app.get("/", (req, res) => {
        res.json({ message: "Welcome to my DB application." });
    });

    //контроллеры
    require("./routes/user.routes")(app);
    require("./routes/task.routes")(app);
    require("./routes/comment.routes")(app);
    require("./routes/run.routes")(app);

    const PORT = 3000;
    // const PORT = process.env.PORT || 3000;
    app.listen(PORT, () => {
        console.log(`Server is running on port ${PORT}.`);
    });
};

start();