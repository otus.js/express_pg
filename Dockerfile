FROM node:alpine
WORKDIR /express_pg
# EXPOSE 3000
COPY package*.json ./
RUN npm ci

# Будем собирать наше ПО
COPY . ./
# Прописать команду, которая будет выполняться при старте нашего образа.
CMD [ "node", "index.js" ] 