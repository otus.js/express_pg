module.exports = app => {
    const comments = require("../controllers/comment.controller.js");
  
    var router = require("express").Router();
  
   /**
 * @swagger
 * tags:
 *   name: Comment
 *   description: API for managing comments
 */
/**
 * @swagger
 * /comments:
 *   get:
 *     summary: Get a list of comments.
 *     tags: [Comments]
 *     responses:
 *       '200':
 *         description: A list of comments.
 */
    router.get("/", comments.findAllasync);
   
/**
 * @swagger
 * /comments/{id}:
 *   get:
 *     summary: Get comment by ID.
 *     tags: [Comments]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the comments.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/:id", comments.findOneAsync);


  /**
 * @swagger
 * /comments/findUserComment/{userId}:
 *   get:
 *     summary: Get comment by ID.
 *     tags: [Comments]
 *     parameters:
 *     - in: path
 *       name: userId
 *       required: true
 *       description: ID of the users in comments.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/findUserComment/:userId", comments.findUserCommentAsync);

  /**
 * @swagger
 * /comments/findTaskComment/{taskId}:
 *   get:
 *     summary: Get comment by ID.
 *     tags: [Comments]
 *     parameters:
 *     - in: path
 *       name: taskId
 *       required: true
 *       description: ID of the users in comments.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/findTaskComment/:taskId", comments.findTaskCommentAsync);


    //Удаление по ID
/**
 * @swagger
 * /comments/{id}:
 *   delete:
 *     summary: Delete comment by ID.
 *     tags: [Comments]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the comment.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comment.
 */
     router.delete("/:id", comments.delete);


/**
 * @swagger
 * /comments:
 *   post: 
 *     summary: Create a JSONPlaceholder comments.
 *     tags: [Comments]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               text:
 *                 type: string
 *                 description: The task's text.
 *               userId:
 *                 type: string
 *               taskId:
 *                 type: string             
 *     responses:
 *       201:
 *         description: A single task.
*/
router.post('/', comments.create);


/**
 * @swagger
 * /comments/{id}:
 *   put: 
 *     summary: Update a JSONPlaceholder comment.
 *     tags: [Comments]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
*     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               text:
 *                 type: string
 *                 description: The task's code.
 *               userId:
 *                 type: string
 *               taskId:
 *                 type: string
 *     responses:
 *       201:
 *         description: A single user.
*/
    router.put("/:id", comments.update)

    app.use("/comments", router);
  };