module.exports = app => {
    const tasks = require("../controllers/task.controller.js");
  
    var router = require("express").Router();
  
   /**
 * @swagger
 * tags:
 *   name: Tasks
 *   description: API for managing tasks
 */
/**
 * @swagger
 * /tasks:
 *   get:
 *     summary: Get a list of users.
 *     tags: [Tasks]
 *     responses:
 *       '200':
 *         description: A list of users.
 */
    router.get("/", tasks.findAllasync);
   
   /**
 * @swagger
 * /tasks/{id}:
 *   get:
 *     summary: Get user by ID.
 *     tags: [Tasks]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
    router.get("/:id", tasks.findOneAsync);
  //по сложности
    /**
 * @swagger
 * /tasks/find/{complexity}:
 *   get:
 *     summary: Get tasks by complexity.
 *     tags: [Tasks]
 *     parameters:
 *     - in: path
 *       name: complexity
 *       required: true
 *       description: login of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single task.
 */
    router.get('/find/:complexity', tasks.findcomplexity);

    //Удаление по ID
/**
 * @swagger
 * /tasks/{id}:
 *   delete:
 *     summary: Delete user by ID.
 *     tags: [Tasks]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
     router.delete("/:id", tasks.delete);


/**
 * @swagger
 * /tasks:
 *   post: 
 *     summary: Create a JSONPlaceholder task.
 *     tags: [Tasks]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               text:
 *                 type: string
 *                 description: The task's code.
 *               inputpar:
 *                 type: string
 *               outputpar:
 *                 type: string
 *               complexity:
 *                 type: string
 *               correct_code:
 *                 type: string
 *     responses:
 *       201:
 *         description: A single user.
*/
router.post('/', tasks.create);


/**
 * @swagger
 * /tasks/{id}:
 *   put: 
 *     summary: Update a JSONPlaceholder task.
 *     tags: [Tasks]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
*     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               text:
 *                 type: string
 *                 description: The task's code.
 *               inputpar:
 *                 type: string
 *               outputpar:
 *                 type: string
 *               complexity:
 *                 type: string
 *               correct_code:
 *                 type: string
 *     responses:
 *       201:
 *         description: A single user.
*/
    router.put("/:id", tasks.update)

    app.use("/tasks", router);
  };