module.exports = app => {
    const users = require("../controllers/user.controller.js");
  
    var router = require("express").Router();
  
   /**
 * @swagger
 * tags:
 *   name: Users
 *   description: API for managing users
 */
/**
 * @swagger
 * /users:
 *   get:
 *     summary: Get a list of users.
 *     tags: [Users]
 *     responses:
 *       '200':
 *         description: A list of users.
 */
    router.get("/", users.findAllasync);
   
   /**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Get user by ID.
 *     tags: [Users]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
    router.get("/:id", users.findOneAsync);
  
    router.get("/find/:login/:password", users.findloginPasswordAsync);
  
    /**
 * @swagger
 * /users/find/{login}:
 *   get:
 *     summary: Get user by ID.
 *     tags: [Users]
 *     parameters:
 *     - in: path
 *       name: login
 *       required: true
 *       description: login of the user.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
    router.get('/find/:login', users.findlogin);

    //Удаление по ID
/**
 * @swagger
 * /users/{id}:
 *   delete:
 *     summary: Delete user by ID.
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single user.
 */
     router.delete("/:id", users.delete);


/**
 * @swagger
 * /users:
 *   post:
 *     summary: Create a JSONPlaceholder task.
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               login:
 *                 type: string
 *                 description: The task's code.
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       201:
 *         description: A single user.
*/
router.post('/', users.create);

// Update a user with id
/**
 * @swagger
 * /users/{id}:
 *   put:
 *     summary: Delete user by ID.
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user.
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               username:
 *                 type: string
 *               login:
 *                 type: string
 *                 description: The task's code.
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       201:
 *         description: A single user.
*/
    router.put("/:id", users.update)

    app.use("/users", router);
  };