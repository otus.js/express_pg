module.exports = app => {
    const runs = require("../controllers/run.controller.js");
  
    var router = require("express").Router();
  
   /**
 * @swagger
 * tags:
 *   name: Run
 *   description: API for managing runs
 */
/**
 * @swagger
 * /runs:
 *   get:
 *     summary: Get a list of runs.
 *     tags: [Runs]
 *     responses:
 *       '200':
 *         description: A list of comments.
 */
    router.get("/", runs.findAllasync);
   
/**
 * @swagger
 * /runs/{id}:
 *   get:
 *     summary: Get run by ID.
 *     tags: [Runs]
 *     parameters:
 *     - in: path
 *       name: id
 *       required: true
 *       description: ID of the runs.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/:id", runs.findOneAsync);

 //Удаление по ID
/**
 * @swagger
 * /runs/{id}:
 *   delete:
 *     summary: Delete run by ID.
 *     tags: [Runs]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the run.
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comment.
 */
router.delete("/:id", runs.delete);


  /**
 * @swagger
 * /runs/findUserRun/{userId}:
 *   get:
 *     summary: Get runs by UserID.
 *     tags: [Runs]
 *     parameters:
 *     - in: path
 *       name: userId
 *       required: true
 *       description: ID of the users in runs.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/findUserRun/:userId", runs.findUserRunAsync);

  /**
 * @swagger
 * /runs/findTaskRun/{taskId}:
 *   get:
 *     summary: Get runs by TaskId.
 *     tags: [Runs]
 *     parameters:
 *     - in: path
 *       name: taskId
 *       required: true
 *       description: ID of the users in runs.
 *       schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: A single comments.
 */
    router.get("/findTaskRun/:taskId", runs.findTaskRunAsync);



/**
 * @swagger
 * /runs:
 *   post: 
 *     summary: Create a JSONPlaceholder comments.
 *     tags: [Runs]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               usercode:
 *                 type: string
 *                 description: The task's text.
 *               language:
 *                 type: string
 *                 description: The task's text.
 *               hashcode:
 *                 type: string
 *                 description: The task's text.
*               userId:
 *                 type: string
 *               taskId:
 *                 type: string             
 *     responses:
 *       201:
 *         description: A single task.
*/
router.post('/', runs.create);

/**
 * @swagger
 * /runs/{id}:
 *   put: 
 *     summary: Update a JSONPlaceholder runs.
 *     tags: [Runs]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the runs.
*     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               usercode:
 *                 type: string
 *                 description: The task's text.
 *               language:
 *                 type: string
 *                 description: The task's text.
 *               hashcode:
 *                 type: string
 *                 description: The task's text.
*               userId:
 *                 type: string
 *               taskId:
 *                 type: string             
 *     responses:
 *       201:
 *         description: A single user.
*/
    router.put("/:id", runs.update)

    app.use("/runs", router);
  };