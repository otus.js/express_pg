const DS = require('../datasource.js').default;
const Tasks = DS.getRepository('Task'); 
//все
 exports.findAllasync = async (req, res) => {
  const result = await Tasks.find({            
               order: { title: 'ASC' }
           });
           res.send(result);
           console.log(result);
       };
//по ID    
exports.findOneAsync =  async (req, res) => {
  const id = req.params.id;
  const result = await Tasks.findOne({
            where: {
                id: id
            }
        })
  .then(data => {
     res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving User with id=" + id
    });
  });
};    

exports.findcomplexity =  async (req, res) => {
    const result = await Tasks.find(
        {
            where: {
                complexity: req.params.complexity
            }
        }
    )
    .then(data => {
        res.send(data);
     })
    .catch(err => {
        res.status(500).send({
          message: "Error retrieving User with id=" + id
        });
      });
    };  

    exports.delete =  async (req, res) => {
        await Tasks.delete(
            {
                id: req.params.id,
            });
        res.send('OK');
      };

      exports.create = async (req, res) => {
        // Validate request
        if (!req.body.title || !req.body.text || !req.body.correct_code || !parseInt(req.body.complexity)) {
          res.status(400).send({
            message: "Content can not be empty!"
          });
          return;
        }
             
        // Create a Task
        const task = {
            title: req.body.title,
            text: req.body.text,
            correct_code: req.body.correct_code,
            inputpar: req.body.inputpar,
            outputpar: req.body.outputpar,
            complexity: req.body.complexity,
        };
        try {
            await Tasks.insert(task);
          res.send('OK');
        } 
         catch (error) {
            console.log(error);
            res.send(error);
        } 
      };

      exports.update = async (req, res) => {
        try {
          const repo = await Tasks.findOneBy({
              id: req.params.id,
          });
          Tasks.merge(repo, req.body);
          const results = await Tasks.save(repo);
          return res.send(results);
      }
      catch (error) {
          res.send(error);
      }
    }