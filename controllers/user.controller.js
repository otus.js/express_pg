const DS = require('../datasource.js').default;
const { MoreThan, Like, createConnection } = require('typeorm'); //+
const Users = DS.getRepository('User'); 
//все
 exports.findAllasync = async (req, res) => {
  const result = await Users.find({            
               order: { username: 'ASC' }
           });
           res.send(result);
           console.log(result);
       };
//по ID    
exports.findOneAsync =  async (req, res) => {
  const id = req.params.id;
  const result = await Users.findOne({
            where: {
                id: id
            }
        })
  .then(data => {
     res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving User with id=" + id
    });
  });
};    

//Поис по логину и паролю
exports.findloginPasswordAsync =  async (req, res) => {
  const login = req.params.login;
  const passw = req.params.password;

  const result = await Users
      .createQueryBuilder("user")
      .where("trim(BOTH FROM user.login) = :login ", { login: login})
      .andWhere("trim(BOTH FROM user.password) = :password", { password: passw})
      .getCount();
      if (result == 1) res.send('true');
      else res.send('false');
}; 

//Поис по логину
exports.findlogin = async (req, res) => {
  const login = req.params.login;
  const result = await Users
      .createQueryBuilder("user")
      .where("trim(BOTH FROM user.login) = :login", { login: login })
      .getOneOrFail();
  res.send(result);
};   

exports.delete =  async (req, res) => {
  await Users.delete(
      {
          id: req.params.id,
      });
  res.send('OK');
};

exports.create = (req, res) => {
  // Validate request
  if (!req.body.username || !req.body.login || !req.body.password || !req.body.email) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a User
  const user = {
    username: req.body.username,
    login: req.body.login,
    password: req.body.password,
    email: req.body.email,
  };
  try {
    Users.insert(user);
    res.send('OK');
  } 
   catch (error) {
      console.log(error);
      res.send(error);
  } 
};

exports.update = async(req, res) => {
  const id = req.params.id;
  try {
    const repo = await Users.findOneBy({
        id: id,
    });
    Users.merge(repo, req.body);
    const results = await Users.save(repo);
    return res.send(results);
}
catch (error) {
    res.send(error);
}
};
