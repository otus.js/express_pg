const DS = require('../datasource.js').default;
const { MoreThan, Like, createConnection } = require('typeorm'); //+
const Runs = DS.getRepository('Run'); 
//все
 exports.findAllasync = async (req, res) => {
  const result = await Runs.find({            
               order: { dt: 'DESC' }
           });
           res.send(result);
           console.log(result);
       };
//по ID    
exports.findOneAsync =  async (req, res) => {
  const id = req.params.id;
  const result = await Runs.findOne({
            where: {
                id: id
            }
        })
  .then(data => {
     res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Comment with id=" + id
    });
  });
};    

//Поис выполненных заданий пользователя 
exports.findUserRunAsync =  async (req, res) => {
  const userId = req.params.userId;
  const result = await Runs.find({
    where: {
        userId: userId
    }
    })
    .then(data => {
        res.send(data);  })
    .catch(err => {
        res.status(500).send({
        message: "Error retrieving Comment with userid=" + id
        });
    });
};
  
//Поиск решений задания пользователями
exports.findTaskRunAsync =  async (req, res) => {
    const taskId = req.params.taskId;

        const result = await Runs.find({
            where: {
                taskId: taskId
            }
        })
  .then(data => {
     res.send(data);  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Comment with id=" + id
    });
  });
};

exports.delete =  async (req, res) => {
  await Runs.delete(
      {
          id: req.params.id,
      });
  res.send('OK');
};

exports.create = (req, res) => {
  // Validate reques
  if (!req.body.userId || !req.body.taskId || !req.body.language || !req.body.usercode  ) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Comment
  const run = {
    usercode: req.body.usercode,
    language: req.body.language,
    hashcode: req.body.hashcode,
    userId: req.body.userId,
    taskId: req.body.taskId,
  };
  try {
    Runs.insert(run);
    res.send('OK');
  } 
   catch (error) {
      console.log(error);
      res.send(error);
  } 
};

exports.update = async(req, res) => {
  const id = req.params.id;
  try {
    const repo = await Runs.findOneBy({
        id: id,
    });
    Runs.merge(repo, req.body);
    const results = await Runs.save(repo);
    return res.send(results);
}
catch (error) {
    res.send(error);
}
 };
