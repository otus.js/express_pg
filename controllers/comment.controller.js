const DS = require('../datasource.js').default;
const { MoreThan, Like, createConnection } = require('typeorm'); //+
const Comments = DS.getRepository('Comment'); 
//все
 exports.findAllasync = async (req, res) => {
  const result = await Comments.find({            
               order: { dt: 'DESC' }
           });
           res.send(result);
           console.log(result);
       };
//по ID    
exports.findOneAsync =  async (req, res) => {
  const id = req.params.id;
  const result = await Comments.findOne({
            where: {
                id: id
            }
        })
  .then(data => {
     res.send(data);
  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Comment with id=" + id
    });
  });
};    

//Поис комментариев пользователя   ERROR
exports.findUserCommentAsync =  async (req, res) => {
  const userId = req.params.userId;
  const result = await Comments.find({
    where: {
        userId: userId
    }
})
.then(data => {
res.send(data);  })
.catch(err => {
res.status(500).send({
message: "Error retrieving Comment with userid=" + id
});
});
};
  
//Поиск комментариев пользователя
exports.findTaskCommentAsync =  async (req, res) => {
    const taskId = req.params.taskId;

        const result = await Comments.find({
            where: {
                taskId: taskId
            }
        })
  .then(data => {
     res.send(data);  })
  .catch(err => {
    res.status(500).send({
      message: "Error retrieving Comment with id=" + id
    });
  });
};

exports.delete =  async (req, res) => {
  await Comments.delete(
      {
          id: req.params.id,
      });
  res.send('OK');
};

exports.create = (req, res) => {
  // Validate request
  if (!req.body.userId || !req.body.taskId) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Comment
  const comment = {
    text: req.body.text,
    userId: req.body.userId,
    taskId: req.body.taskId
  };
  try {
    Comments.insert(comment);
    res.send('OK');
  } 
   catch (error) {
      console.log(error);
      res.send(error);
  } 
};

exports.update = async(req, res) => {
  const id = req.params.id;
  try {
    const repo = await Comments.findOneBy({
        id: id,
    });
    Comments.merge(repo, req.body);
    const results = await Comments.save(repo);
    return res.send(results);
}
catch (error) {
    res.send(error);
}
};
