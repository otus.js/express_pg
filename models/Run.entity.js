const EntitySchema = require('typeorm').EntitySchema;

module.exports = new EntitySchema({
    name: "Run",
    tableName: "runs",
    columns: {
        id: {
            primary: true,
            type: "int"
        },
        usercode: {
            type: "varchar",
            length: 1000,
            nullable: false
        },
        language:{
            type: "varchar",
            length: 50,
            nullable: false
        },
        hashcode: {
            type: "varchar",
            length: 500,
            nullable: true
        },
        userId: {
            type: 'int',
            nullable: false
          },
        taskId: {
            type: "int",
            nullable: false
        },
        dt: {
            type: 'date',
            require:false,
        },
    },
    //  relations: {
    //     userrun: {
    //         target: 'User',
    //         type: 'many-to-one',
    //         joinTable: true,
    //         cascade: true,
    //         eager: true,
    //       },
    //       taskrun: {
    //         target: 'Task',
    //         type: 'many-to-one',
    //         joinTable: true,
    //         cascade: true,
    //         eager: true,
    //      },
    //  }
});
