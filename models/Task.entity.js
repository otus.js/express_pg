const EntitySchema = require('typeorm').EntitySchema;

module.exports = new EntitySchema({
    name: "Task",
    tableName: "tasks",
    columns: {
        id: {
            primary: true,
            type: "int",
        },
        title: {
            type: "varchar",
            length: 50,
            nullable: false,
        },
        text: {
            type: "varchar",
            length: 500,
            nullable: false,
        },
        inputpar: { 
            type: "varchar",
            length: 200,
            nullable: true
        },
        outputpar: { 
            type: "varchar",
            length: 200,
            nullable: true
        },
        complexity:{
            type: "int",
            nullable: true
        },
        correct_code: {
            type: "varchar",
            length: 1000,
            nullable: false
        },
    },
    // relations: {
    //     country: {
    //         type: 'one-to-one',
    //         target: 'Country',
    //         eager: true,
    //         joinColumn: {
    //             name: 'country_id'
    //         }
    //     }
    // }

});