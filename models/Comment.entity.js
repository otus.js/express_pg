const EntitySchema = require('typeorm').EntitySchema;

module.exports = new EntitySchema({
    name: "Comment",
    tableName: "comments",
    columns: {
        id: {
            primary: true,
            type: "int"
        },
        text: {
            type: "varchar",
            length: 1000,
            nullable: false
        },
        userId: {
            type: 'int',
            nullable: false
          },
        taskId: {
            type: "int",
            nullable: false
        },
        dt: {
            type: 'date',
            require:false,
        },
    },
     relations: {
        user: {
            target: 'User',
            type: 'many-to-one',
            joinTable: true,
            cascade: true,
            eager: true,
          },
          task: {
            target: 'Task',
            type: 'many-to-one',
            joinTable: true,
            cascade: true,
            eager: true,
         },
     }
});
