const EntitySchema = require('typeorm').EntitySchema;

module.exports = new EntitySchema({
    name: "User",
    tableName: "users",
    columns: {
        id: {
            primary: true,
            type: 'int'
        },
        username: {
            type: 'varchar',
            length: 500,
            nullable: false
        },
        login:{
            type: 'varchar',
            length: 50,
            nullable: false
        },
        email: {
            type: 'varchar',
            length: 100,
            nullable: false
        },
        password: {
            type: 'varchar',
            length: 30,
            nullable: false
        },
    },
      relations: {
        comments: {
          target: 'Comment', 
          type: 'one-to-many',
          inverseSide: 'user'
        },
      },
});

